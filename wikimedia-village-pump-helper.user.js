// ==UserScript==
// @name		Wikimedia Village Pump helper
// @description	Offer stock answers on Wikimedia Village Pumps above the comment field when using the classic wikitext editor.
// @author		Andre Klapper <ak-47@gmx.net>
// @version		2013-09-20
// @include		https://commons.wikimedia.org/w/index.php?title=Commons:Forum&action=edit*
// @include		https://commons.wikimedia.org/w/index.php?title=Commons:Forum&action=submit
// @include		https://commons.wikimedia.org/w/index.php?title=Commons:Upload_Wizard_feedback&action=edit*
// @include		https://commons.wikimedia.org/w/index.php?title=Commons:Upload_Wizard_feedback&action=submit
// @include		https://commons.wikimedia.org/w/index.php?title=Commons:Village_pump&action=edit*
// @include		https://commons.wikimedia.org/w/index.php?title=Commons:Village_pump&action=submit
// @include		https://en.wikibooks.org/w/index.php?title=Wikibooks:Reading_room/General&action=edit*
// @include		https://en.wikibooks.org/w/index.php?title=Wikibooks:Reading_room/General&action=submit
// @include		https://en.wikinews.org/w/index.php?title=Wikinews:Water_cooler/technical&action=edit*
// @include		https://en.wikinews.org/w/index.php?title=Wikinews:Water_cooler/technical&action=submit
// @include		https://de.wikipedia.org/w/index.php?title=Wikipedia:Fragen_zur_Wikipedia&action=edit*
// @include		https://de.wikipedia.org/w/index.php?title=Wikipedia:Fragen_zur_Wikipedia&action=submit
// @include		https://en.wikipedia.org/w/index.php?title=Wikipedia:Help_desk&action=edit&*
// @include		https://en.wikipedia.org/w/index.php?title=Wikipedia:Help_desk&action=submit
// @include		https://en.wikipedia.org/w/index.php?title=Wikipedia:Village_pump_(miscellaneous)&action=edit*
// @include		https://en.wikipedia.org/w/index.php?title=Wikipedia:Village_pump_(miscellaneous)&action=submit
// @include		https://en.wikipedia.org/w/index.php?title=Wikipedia:Village_pump_(technical)&action=edit*
// @include		https://en.wikipedia.org/w/index.php?title=Wikipedia:Village_pump_(technical)&action=submit
// @include		https://de.wikisource.org/w/index.php?title=Wikisource:Skriptorium&action=edit*
// @include		https://de.wikisource.org/w/index.php?title=Wikisource:Skriptorium&action=submit
// @include		https://en.wikisource.org/w/index.php?title=Wikisource:Scriptorium&action=edit*
// @include		https://en.wikisource.org/w/index.php?title=Wikisource:Scriptorium&action=submit
// @include		https://en.wikiversity.org/w/index.php?title=Wikiversity:Colloquium&action=edit*
// @include		https://en.wikiversity.org/w/index.php?title=Wikiversity:Colloquium&action=submit
// @include		https://en.wikivoyage.org/w/index.php?title=Wikivoyage:Travellers'_pub&action=edit*
// @include		https://en.wikivoyage.org/w/index.php?title=Wikivoyage:Travellers'_pub&action=submit
// @include		https://en.wiktionary.org/w/index.php?title=Wiktionary:Grease_pit/20*/*&action=edit*
// @include		https://en.wiktionary.org/w/index.php?title=Wiktionary:Grease_pit/20*/*&action=submit
// @include		https://meta.wikimedia.org/w/index.php?title=Wikimedia_Forum&action=edit*
// @include		https://meta.wikimedia.org/w/index.php?title=Wikimedia_Forum&action=submit
// @include		https://pl.wikipedia.org/w/index.php?title=Wikipedia:Kawiarenka/Kwestie_techniczne&action=edit*
// @grant		none
// @copyright		(C) Copyright Control by the authors, 2007 and later.
// @license		The contents of this file are dual-licensed under the Mozilla Public License 2.0 (MPL-2.0)
//                      and the GNU General Public License (GPL-2.0) Version 2 or later.
//			A small part of this code comes from the previous GNOME Bugzilla 2.20 version located at
//			http://git.gnome.org/cgit/bugzilla-newer/tree/template/en/default/bug/edit.html.tmpl
//			The Initial Developer of the Original Code of that part is Netscape Communications
//			Corporation. Portions created by Netscape are
//			Copyright (C) 1998 Netscape Communications Corporation.
//			Contributor(s): Gervase Markham <gerv@gerv.net>
//			                Vaskin Kissoyan <vkissoyan@yahoo.com>
// ==/UserScript==


/******************************************************************
TODO:
* Cover:
** ?debug=true
** ?useskin=monobook
** ?uselang=qqx
** ?safemode=1
******************************************************************/

/* Get the subdomain, e.g. 'en' in 'en.wikipedia.org', to show language specific answers */
var URLHost = window.location.host;
URLHost = URLHost.substr(0,URLHost.indexOf('.'));
URLHost = URLHost.substr(URLHost.indexOf('.')+1);

/* get textarea field in MediaWiki in which to insert the answer */
if (document.getElementById("wpTextbox1") !== null) {
  var comment_textfield = document.getElementById("wpTextbox1");
}
else {
  window.alert("Error: Could not find element 'wpTextbox1' on this page");
}

/*************************************************************/
/* set prefered height of comment field                      */
/*************************************************************/
comment_textfield.rows = 15;

/*** helper function to create container elements (e.g. <div>) that we can drop random text or links into
 *  @param ElementToCreate the element (e.g. a <div>) that we plan to put somewhere
 *  @param place element after which this item is added
 *  @param ID a random ID parameter
 *  @param heading optional text header to display at the beginning of the element
 *  @param style CSS style for this item
 *  @return void
 */
function createContainer(ElementToCreate, place, ID, heading, style) {
  ElementToCreate.setAttribute('id', ID);
  ElementToCreate.appendChild(document.createTextNode(heading));
  ElementToCreate.setAttribute('style', style);
  place.parentNode.insertBefore(ElementToCreate, place);
}

/*******************************************************************/
/* create <div> container for stock answers above comment input field */
/*******************************************************************/
  var stockAnswersContainerDiv = document.createElement("div"); // most be defined here globally as we need it later to insert items in it
createContainer(stockAnswersContainerDiv, comment_textfield, "stock_answers_div", "Stock answers:  ", "max-width:940px; border:0px solid #000000; padding:0px; margin: 2px 0px 2px; text-align: left; background-color:#f4f4f4");

/**************************************************************************************/
/***helper function that creates a stock answers in the "stockAnswersContainerDiv" <div> 
 *  that we created earlier, located above the comment input field
 *  @param element type of element to create (e.g. <span> or <div>)
 *  @param cclass class for the element to create
 *  @param ID some ID for the element to create
 *  @param text the text to be displayed 
 *  @param clickHandler name of the EventHandler function; makes only sense if this is a stock response (function must have been defined before)
 *  @param style CSS style for this item
 *  @param place element after which this item is added
 *  @return void
 */
function createItemInContainer(element, cclass, ID, text, clickHandler, style, place) {
    var SpanContainer = document.createElement(element);
    SpanContainer.setAttribute('class', cclass);
    SpanContainer.setAttribute('id', ID);
    var SpanContainerText = document.createTextNode(text);
    SpanContainer.appendChild(SpanContainerText);
    place.appendChild(SpanContainer);
    if (style == 1) {
      SpanContainer.setAttribute('style', "padding:0px 5px 0px 5px; background-color:#8ae234; color: #000000;");
    }
    else if (style == 2) {
      SpanContainer.setAttribute('style', "padding:0px 5px 0px 5px; background-color:#fce94f; color: #000000;");
    }
    else if (style == 3) {
      SpanContainer.setAttribute('style', "padding:0px 5px 0px 5px; background-color:#edd400; color: #000000;");
    }
    else if (style == 4) {
      SpanContainer.setAttribute('style', "padding:0px 5px 0px 5px; background-color:#fcaf3e; color: #000000;");
    }
    else if (style == 5) {
      SpanContainer.setAttribute('style', "padding:0px 5px 0px 5px; background-color:#e9b96e; color: #000000;");
    }
    else if (style == 6) {
      SpanContainer.setAttribute('style', "padding:0px 5px 0px 5px; background-color:#ef2929; color: #000000;");
    }
    else if (style == 7) {
      SpanContainer.setAttribute('style', "padding:0px 5px 0px 5px; background-color:#729fcf; color: #000000;");
    }
    else if (style == 8) {
      SpanContainer.setAttribute('style', "padding:0px 5px 0px 5px; background-color:#3465a4; color: #FFFFFF;");
    }
    else if (style == 9) {
      SpanContainer.setAttribute('style', "padding:0px 5px 0px 5px; background-color:#ad7fa8; color: #000000;");
    }
    else /* if (style == 0) */ {
      SpanContainer.setAttribute('style', "padding:0px 5px 0px 5px; color: #000000;");
    }
    SpanContainer.addEventListener("click", clickHandler, true);
}

/**************************************************************************************/
/* helper function that adds reply text to the comment textarea, enables the CC checkbox
 * and sets keyboard focus to the "submit" button 
 * (shamelessly partially adopted from bugzilla.gnome.org code at 
 * /template/en/default/bug/edit.html.tmpl which is under a Mozilla Public License)
 * @param text text to add as a new comment
 * @param status status of bug report (e.g. "new" or "resolved")
 * @param resolution string that describes the resolution (e.g. "DUPLICATE" or "FIXED")
 * @param ccme if "1", add me to the CC list of the bug
 * @return void
 */
function addTextToComment(text, status, resolution, ccme) {
  text = text.split(/\r|\n/);
  var replytext = "";
  for (var i=0; i < text.length; i++) {
      replytext += text[i]; 
  }
  var textarea = comment_textfield;
  textarea.value += replytext;
  textarea.scrollTop = textarea.scrollHeight;
  return false;
}


/**********************************************************************************************
 * PLEASE NOTE:
 * 1) if you want to add individual stock answer for yourself, you have to add two things:
 *    a) an EventHandler function (like "FooClick") 
 *       to define the comment text and the bug resolution
 *    b) a createItemInContainer() call at the end of this script that creates the stock
 *       response link on the webpage and calls the EventHandler function you defined in a)
 **********************************************************************************************/

/**********************************************************************************************/
/* all those boring *event handlers* that include the text that should be added as a comment: */
/* (scroll down to the end for actually *adding* the stock response link to the page)         */
/**********************************************************************************************/

function GenericThanksClickEn (Event) {
  var Text = "Thanks for taking the time to report this! ";
  addTextToComment(Text, '', '', '1');
  scrollDown();
}

function GenericThanksClickDe (Event) {
  var Text = "Danke für das Berichten dieses Problems! ";
  addTextToComment(Text, '', '', '1');
  scrollDown();
}

function VagueClickEn (Event) {
  var Text = "Unfortunately this is not very useful because it does not describe the problem well. If you have time and can still reproduce the problem, please read [[mw:How to report a bug]] and add a more useful description to this report. ";
  addTextToComment(Text, '', '', '1');
}

function PleaseCopyToPhabricatorGenericEn (Event) {
  var Text = "The problem you are reporting sounds like a potential issue in the code of the MediaWiki software or the server configuration. If the problem is reproducible, it would be nice if somebody who has this issue could create a report in the 'Phabricator' task tracker by following the instructions at [[mw:How to report a bug]]. This is to make developers of the software aware of the issue. If you have done so, please paste the number of the bug report (or the link) here, so others can also inform themselves about the bug's status. Thanks in advance! ";
  addTextToComment(Text, '', '', '1');
}

function PleaseCopyToPhabricatorGenericDe (Event) {
  var Text = "Das Problem klingt nach einem möglichen Fehler im Code der MediaWiki-Software oder der Konfiguration der Wikimedia-Server. Falls das Problem reproduzierbar ist, so wäre es nett dies zunächst in der [[:w:de:Wikipedia:Technik/Werkstatt|Technik-Werkstatt]] zu klären.\nNach Prüfung kann das Problem in englischer Sprache in die 'Phabricator'-Fehlerdatenbank eintragen werden (siehe [[mw:How to report a bug/de]]). Dies macht den Softwareentwicklern das Problem bewusst. Bitte danach hier die Nummer oder den Link zum Fehlerbericht einstellen, damit andere sich auch über den Status informieren können. Danke im voraus! ";
  addTextToComment(Text, '', '', '1');
}

function CopiedToPhabricatorEn (Event) {
  var Text = "{{tracked|XXXXX}}\nThe problem you are reporting sounds like an issue in the code of the MediaWiki software or the server configuration. I have copied the issue to the 'Phabricator' bug tracker (anybody can do this, see [[mw:How to report a bug]] for more info). This is to make developers of the software aware of the issue. The link and number of the bug report is on the right, so everybody can inform themselves about status and progress. ";
  addTextToComment(Text, '', '', '1');
}

function VisualEditorIssueEn (Event) {
  var Text = "For future reference, [[WP:VE/F]] is the best place to give feedback for the VisualEditor (or to check for existing threads requesting the same feature/bugfix) so the developers can see it. Hope that helps.";
  addTextToComment(Text, '', '', '1');
}

function Signature (Event) {
  var Text = "--~~~~";
  addTextToComment(Text, '', '', '1');
}

function Colon (Event) {
  var Text = ":";
  addTextToComment(Text, '', '', '1');
}

/*******************************************************************/
/* now finally add our custom stock response links to the web page */
/* PATTERN: createItemInContainer('span', 'class' 'ID',            */
/*   '[shownText]', ClickHandlerName, colorNumber, place);         */
/* clickHandlerName must be defined above!                         */
/*******************************************************************/

createItemInContainer('span', null, '', '[:]', Colon, 1, stockAnswersContainerDiv);
if (URLHost == "de") {
  createItemInContainer('span', null, '', '[Thanks!]', GenericThanksClickDe, 1, stockAnswersContainerDiv);
  createItemInContainer('span', null, '', '[Copy to Phab Generic]', PleaseCopyToPhabricatorGenericDe, 3, stockAnswersContainerDiv);
  createItemInContainer('span', null, '', '[Toolserver/Labs-Issues]', ToolserverVsLabsCriticismDe, 4, stockAnswersContainerDiv);
}
else if (URLHost == "en") {
  createItemInContainer('span', null, '', '[Thanks!]', GenericThanksClickEn, 1, stockAnswersContainerDiv);
  createItemInContainer('span', null, '', '[Description Vague]', VagueClickEn, 2, stockAnswersContainerDiv);
  createItemInContainer('span', null, '', '[Copy to Phab Generic]', PleaseCopyToPhabricatorGenericEn, 3, stockAnswersContainerDiv);
  createItemInContainer('span', null, '', '[Copied to Phab]', CopiedToPhabricatorEn, 3, stockAnswersContainerDiv);
  createItemInContainer('span', null, '', '[VisualEditor-Issues]', VisualEditorIssueEn, 4, stockAnswersContainerDiv);
}
createItemInContainer('span', null, '',  '[Signature]', Signature, 6, stockAnswersContainerDiv);
